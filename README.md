# CSC232 - Data Structures with C++

## Missouri State University, Spring 2017

### Lecture 2: 20 Jan 2017

### Agenda

* Questions?
* Appendix A.1 - Language Basics
    * A.1.1: Comments
    * A.1.2: Identifiers and Keywords
    * A.1.3: Primitive Data Types
    * A.1.4: Variables
    * A.1.5: Literal Constants
    * A.1.6: Named Constants
    * A.1.7: Enumerations
    * A.1.8: The `typedef` Statement
    * A.1.9: Assignments and Expressions
* Appendix A.2 - Input and Output Using `iostream`
    * A.2.1: Input
    * A.2.2: Output
    * A.2.3: Manipulators
* Appendix A.3 - Functions
    * A.3.1: Standard Functions

### Approach
In this lecture, the above topics are presented with sample code stored in this repo. Specifically, the three major sections may be checked out using the listed repo hash.

### Commands
During the course of this lecture, a number of commands will be issued. Below is a summary of some of them.

#### Compiling
To compile the source file, we issue the following command at the command line prompt (shown here as the $, so don't type the that character when you try this on your own).

To compile this program, we used `g++` as follows:

```c++
$ g++ -std=c++14 -Wall Main.cpp -o VolumeCalculator
```

Notes:

* `g++` is the C++ compiler used to compile our source code
* `-std=c++14` is an example of a switch; it provides additional details on how the executing command (`g++` in this case) operates. In this case, it tells `g++` to use grammar rules associated with C++ 14.
* `-Wall` is another switch; here the `W` instructs `g++` to echo warnings about issues found during the compilation process and the `all` part of it tells it to echo all such warnings. At this stage, we want as much information as possible.
* `Main.cpp` is the source file we're compiling.
* `-o VolumeCalculator` is a switch providing instructions to the linker when building the executable file. In this case, we're naming the executable file that gets built `VolumeCalculator`.

#### Editing
In this lecture, the `emacs` editor is used for making edits to the source file. It is a very powerful editor and we use it to handle a number of `git` related operations as well. When issuing commands in `emacs` you'll often see them listed as starting with one of two different forms:

```
C-
M-
```

The `C-` means to hold the `control` key and type the next character, e.g., `C-x` means to hold the `control` key and type `x`. The `M-` sequence refers to a "meta" key, which on most keyboards is the `esc` (escape) key. So `M-x` means to hold the `esc` character and type `x`.

A **tutorial** on how to use `emacs` is also available from within the editor itself by typing `C-h t` at any time.

To update to the latest version, i.e., **`git pull`**:

```
C-x v +
```

will open a new buffer in emacs an report some like:

```
Running "git pull"...
Already up-to-date.
```

Of coure, if the files had differed with origin, then the output would be different.

To make a **commit**, type:

```
C-x v v
```

You'll be prompted to provide a commit message (Summary). When you're done with the commit message, type

```
C-c C-c
```

to actually commit your changes.

To **push** your changes, type:

```
C-x v P
```

To **save** your changes in a file, type

```
C-x C-s
```

And to **open** a file, type

```
C-x C-f
```

Finally, to **navigate between buffers**, type

```
C-x arrow-key
```

where arrow-key is either left or right arrow key.

If your window is split into more than one buffer, you can type `C-x 1` to make the current buffer occupy the whole window. The character following the `x` is the number one and not the lowercase L.
