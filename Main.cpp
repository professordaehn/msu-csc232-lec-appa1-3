/**
 * CSC232 - Data Structures with C++
 * Missouri State University, Spring 2017
 *
 * @brief  Entry point of our application containing the main function.
 * @file   Main.cpp
 * @author Jim Daehn <jdaehn@missouristate.edu>
 */

#include <cstdlib>
#include <iomanip>
#include <iostream>

/**
 * Entry point to this application.
 * 
 * @param  argc the number of command line arguments, including the executable
 * @param  argv an array of command line arguments, including the executable
 * @return EXIT_SUCCESS is returned upon successful execution.
 * @pre    None; command line arguments are ignored.
 * @post   Control returns to client upon completion with the proper exit
 *         status code.
 */
int main(int argc, char* argv[]) {

  // Computes the volume of a sphere of a given radius
  const double PI = 3.14159;
  double radius;

  std::cout << "Enter the radius of the sphere: ";
  std::cin >> radius;

  double volume = 4 * PI * radius * radius * radius / 3;
  std::cout << std::fixed << std::setprecision(1) << "The volume of a sphere of radius "
	    << radius << " inches is " << volume
	    << " cubic inches.\n";
  
  return EXIT_SUCCESS;
}
